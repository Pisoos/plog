
#ifndef __cpp_modules
#include "plog.h"
#endif 


namespace pspp
{
namespace log
{

#define DECLARE_LOG_FUNCTION(mode) \
	void logger::mode(std::string_view message, const std::source_location loc)\
	{\
		(this->*_ ## mode ## _ptr)(message, &loc);\
	}\
	void logger::mode ## n(std::string_view message, const std::source_location loc)\
	{\
		(this->*_ ## mode ## n_ptr)(message, &loc);\
	}

#define USE_FLAG_FUNCTION(fname) logger::_use_flags* logger::_use_flags::use_ ## fname ## f()\
{\
	_prefix_ ## fname ## _func_ptr = &logger::_prefix_ ## fname ## _;\
	return this;\
}

#define NOT_USE_FLAG_FUNCTION(fname) logger::_use_flags* logger::_use_flags::not_use_ ## fname ## f()\
{\
	_prefix_ ## fname ## _func_ptr = &logger::_prefix_nothing_;\
	return this;\
}

#define LOG_FUNCTION(mode) \
void logger::_ ## mode ## _func(std::string_view message, const std::source_location* loc) \
{\
	std::string* prefix = _get_prefix_str( mode ## f, loc);\
	fmt::print("{}{}{}{}", log_mode_c[mode ## _m] , *prefix , message , log_mode_c[unc]); \
	delete prefix;\
}
//_log(OKLOG, &oklogf, message, "\n", &loc);

#define LOG_N_FUNCTION(mode) \
void logger::_ ## mode ## n_func(std::string_view message, const std::source_location* loc) \
{\
	std::string* prefix = _get_prefix_str( mode ## f, loc);\
	fmt::print("{}{}{}{}\n", log_mode_c[mode ## _m] , *prefix , message , log_mode_c[unc]); \
	delete prefix;\
}

#define SWITCH_LOG_FUNCTION_ON(func_name) \
	_ ## func_name ## _ptr = &logger::_ ## func_name ## _func; \
	_ ## func_name ## n_ptr = &logger::_ ## func_name ## n_func

#define SWITCH_LOG_FUNCTION_OFF(func_name) \
	_ ## func_name ## _ptr = &logger::_log_nothing; \
	_ ## func_name ## n_ptr = &logger::_log_nothing



USE_FLAG_FUNCTION(time);
USE_FLAG_FUNCTION(funcname);
USE_FLAG_FUNCTION(filename);
USE_FLAG_FUNCTION(line);
USE_FLAG_FUNCTION(column);

NOT_USE_FLAG_FUNCTION(time);
NOT_USE_FLAG_FUNCTION(funcname);
NOT_USE_FLAG_FUNCTION(filename);
NOT_USE_FLAG_FUNCTION(line);
NOT_USE_FLAG_FUNCTION(column);

logger::_use_flags* logger::_use_flags::use_locationf()
{
	use_filenamef();
	use_funcnamef();
	use_linef();
	use_columnf();
	return this;

}

logger::_use_flags* logger::_use_flags::not_use_locationf()
{
	not_use_filenamef();
	not_use_funcnamef();
	not_use_linef();
	not_use_columnf();
	return this;
}

std::string logger::_get_time_str() const
{
	time_t timer = time(NULL);
	tm* t = localtime(&timer);

//	auto now = std::chrono::system_clock::now();
//	auto location = std::chrono::zoned_time{};

//	return fmt::format("{} ", now);
	return fmt::format("{}-{}-{} {}:{}:{}", t->tm_year + 1900, 
			t->tm_mon + 1, t->tm_mday,
			t->tm_hour, t->tm_min, t->tm_sec);
}

std::string logger::_prefix_nothing_(const std::source_location* _loc)
{
	return "";
};
std::string logger::_prefix_time_(const std::source_location* _loc)
{
	return fmt::format("{} ", _get_time_str());
	//return _get_time_str() + " ";;
}
std::string logger::_prefix_funcname_(const std::source_location* _loc)
{
	return std::string(_loc->function_name()) +  " ";
	//return fmt::format("{} ", _loc->function_name());
}
std::string logger::_prefix_filename_(const std::source_location* _loc)
{
	return std::string(_loc->file_name()) + " ";
	//return fmt::format("{} ", _loc->file_name());
}
std::string logger::_prefix_column_(const std::source_location* _loc)
{
	return "c:" + std::to_string(_loc->column()) + ": ";
	//return fmt::format("c:{}: ", _loc->column());
}
std::string logger::_prefix_line_(const std::source_location* _loc)
{
	return "l:" + std::to_string(_loc->line()) + ": ";
	//return fmt::format("l:{}:", _loc->line());
}

std::string* logger::_get_prefix_str(const logger::_use_flags* _flags, const std::source_location* _loc)
{
	return new std::string(fmt::format("{}{}{}{}{}{}", (this->*_flags->_prefix_time_func_ptr)(nullptr),
			mode,
			(this->*_flags->_prefix_filename_func_ptr)(_loc),
			(this->*_flags->_prefix_funcname_func_ptr)(_loc),
			(this->*_flags->_prefix_line_func_ptr)(_loc),
			(this->*_flags->_prefix_column_func_ptr)(_loc)));
}

LOG_FUNCTION(ilog);
LOG_FUNCTION(wlog);
LOG_FUNCTION(elog);
LOG_FUNCTION(oklog); 

LOG_N_FUNCTION(ilog);
LOG_N_FUNCTION(wlog); 
LOG_N_FUNCTION(elog); 
LOG_N_FUNCTION(oklog);

DECLARE_LOG_FUNCTION(ilog);
DECLARE_LOG_FUNCTION(wlog);
DECLARE_LOG_FUNCTION(elog);
DECLARE_LOG_FUNCTION(oklog);


void logger::_init_use_flags()
{
	elogf = new _use_flags();
	wlogf = new _use_flags();
	ilogf = new _use_flags();
	oklogf = new _use_flags();
	log = new _use_flags();

	elogf->_init_use_flag();
	wlogf->_init_use_flag();
	ilogf->_init_use_flag();
	oklogf->_init_use_flag();
	log->_init_use_flag();
}

void logger::_use_flags::_init_use_flag()
{
	_prefix_time_func_ptr = &logger::_prefix_nothing_;
	_prefix_filename_func_ptr = &logger::_prefix_nothing_;
	_prefix_funcname_func_ptr = &logger::_prefix_nothing_;
	_prefix_line_func_ptr = &logger::_prefix_nothing_;
	_prefix_column_func_ptr = &logger::_prefix_nothing_;
}


logger::logger()
{
	this->debug_mode = false;
	this->mode = "";
	_init_use_flags();
	log_off();
}
logger::logger(const std::string &_mode)
{
	this->debug_mode = false;
	this->mode = "[" + _mode + "]";
	_init_use_flags();
	log_off();
}

void logger::set_flags_for_all(logger::_use_flags* flags)
{
	*this->ilogf = *flags;
	*this->wlogf = *flags;
	*this->elogf = *flags;
	*this->oklogf = *flags;
}

void logger::log_off()
{
	SWITCH_LOG_FUNCTION_OFF(ilog);
	SWITCH_LOG_FUNCTION_OFF(wlog);
	SWITCH_LOG_FUNCTION_OFF(elog);
	SWITCH_LOG_FUNCTION_OFF(oklog);
}

void logger::log_on()
{
	SWITCH_LOG_FUNCTION_ON(ilog);
	SWITCH_LOG_FUNCTION_ON(wlog);
	SWITCH_LOG_FUNCTION_ON(elog);
	SWITCH_LOG_FUNCTION_ON(oklog);
}

void logger::_log_nothing(std::string_view message, const std::source_location* loc){
}

#undef SWITCH_LOG_FUNCTION_OFF
#undef SWITCH_LOG_FUNCTION_ON
#undef LOG_FUNCTION
#undef LOG_N_FUNCTION
#undef DECLARE_LOG_FUNCTION


} // namespace log
} // namespace pspp
