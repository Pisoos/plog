# plog

Pisoosによる、C++用の行説出力の既句集。

## 構設・移植：

```bash
mkdir build
pushd build
export MY_INSTALL_DIR=$HOME/.local/
cmake .. -DMAKE_INSTALL_PREFIX=$MY_INSTALL_DIR
cmake --build . -j
cmake --install .
```

「/」配下に置く場合、最後の命令句は管理者権限、sudo付きで実行する必要がある。

## 使用例

```c++
#include "plog.h"

int main()
{
	pspp::log::logger l(true, "PLOG_EX");
	
	l.ilogn ("行説情報出力");
	l.elogn ("失敗情報出力");
	l.wlogn ("警告情報出力");
	l.oklogn("成功情報出力");

	return 0;
}
```
